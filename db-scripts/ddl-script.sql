/*
 * Medical System V2
 * by: Carlos, Martha, Delfina, Miguel
 * 
 * === convenciones ===
 * - Minúsculas para los identificadores y mayúsculas para las instrucciones SQL
 * - Los identificadores deben ser en español, sin carácteres especiales (ñ, ó)
 * - El estilo de escribir es Snake case (snake_case)
 *   * ejemplo: CREATE TABLE mi_tabla
 * - En caso de que el identificador suene 'ambiguo' al aplicar las convenciones, 
 *   se corrige en base al sonido de la sílaba conflictiva;
 *   * ejemplo: año => anio
 * - Comenta solo lo necesario, tú código debe ser autoexplicativo
 */

/*
 * Creación del usuario para utilizar la base de datos
 */
CREATE USER 'personal_medico'@'%' IDENTIFIED BY '123qwerty';
GRANT INSERT ON sistema_medico.* TO 'personal_medico'@'%';
GRANT UPDATE ON sistema_medico.* TO 'personal_medico'@'%';
GRANT SELECT ON sistema_medico.* TO 'personal_medico'@'%';
GRANT DELETE ON sistema_medico.* TO 'personal_medico'@'%';
GRANT EXECUTE ON sistema_medico.* TO 'personal_medico'@'%';
FLUSH PRIVILEGES;

/*
 * Creación de la base de datos
 */
CREATE DATABASE sistema_medico;

USE sistema_medico;

--
-- Usuario
--

/* Tabla única para la clase base, Usuario, que incluye todos los atributos
 * comunes que heredarán el resto de las tablas.
 */
CREATE TABLE usuario(
	correo_electronico VARCHAR(35) NOT NULL,
	contrasena VARCHAR(20) NOT NULL,
	tipo VARCHAR(15) NOT NULL,
	
	PRIMARY KEY(correo_electronico)
);

--
-- Persona 
--

/* Tabla única para la subclase base, Persona, que incluye todos los atributos
 * comunes que heredarán el resto de las tablas.
 */
CREATE TABLE persona(
    id_persona INT NOT NULL AUTO_INCREMENT,
	correo_electronico VARCHAR(35),
    apellido_paterno_p VARCHAR(40) NOT NULL,
    apellido_materno_p VARCHAR(40) NULL,
    nombre VARCHAR(40) NOT NULL,
    edad INT(3) NOT NULL,
    sexo VARCHAR(1) NOT NULL,
    telefono VARCHAR(10),
    
    PRIMARY KEY(id_persona),
	FOREIGN KEY(correo_electronico) REFERENCES usuario(correo_electronico) ON DELETE CASCADE
);



--
-- Enfermedades heredofamiliares
--

/*
 * Tabla que contendrá las enfermedades heredofamiliares del paciente
 */
CREATE TABLE enfermedades_heredofamiliares(
    id_enfermedad_heredofamiliar INT NOT NULL AUTO_INCREMENT,
    enfermedades_mentales VARCHAR(55) NOT NULL,
    hipercolesterolemia VARCHAR(2) NOT NULL,
    diabetes VARCHAR(2) NOT NULL,
    hipertension VARCHAR(2) NOT NULL,
    cancer VARCHAR(2) NOT NULL,
    otro VARCHAR(255),

    PRIMARY KEY(id_enfermedad_heredofamiliar)
);

--
-- Patológicos personales
--

/*
 * Tabla que contendrá las enfermedades patológicas del paciente.
 */
CREATE TABLE patologicos_personales(
    id_patologico_personal INT NOT NULL AUTO_INCREMENT,
    enfermedades_personales VARCHAR(255) NOT NULL,
    enfermedades_cronicas VARCHAR(255) NOT NULL,
    tratamientos_anteriores VARCHAR(255) NOT NULL,
    alergias VARCHAR(255) NOT NULL,

    PRIMARY KEY(id_patologico_personal)
); 

--
-- Ginecológicos
--

/*
 * Tabla que contendrá los datos ginecológicos del paciente.
 * ESTA TABLA SE CREARÁ SOLO SI EL PACIENTE ES FEMENINO
 */
CREATE TABLE ginecologicos(
    id_ginecologico INT NOT NULL AUTO_INCREMENT,
    menarca DATE,
    ritmo_menstrual VARCHAR(255),
    alteraciones_menstruales VARCHAR(255),
    gestas INT NOT NULL,
    cesareas INT NOT NULL,
    partos INT NOT NULL,
    abortos INT NOT NULL,
    anticonceptivos VARCHAR(255),
    inicio_menopausia DATE,
    trastornos_menopausicos VARCHAR(255),

    PRIMARY KEY(id_ginecologico)
);

--
-- Antecedentes
--
/* Tabla de que se creará junto a tabla Paciente, almacenará
 * todo lo relacionado con los antecedentes del paciente.
 * A la vez, esta tabla servirá como pivote para las tablas:
 * enfermedades_heredofamiliares, ginecologicos y patologicos_personales.
 */
CREATE TABLE antecedentes(
    id_antecedente INT NOT NULL AUTO_INCREMENT,
    id_enfermedad_heredofamiliar INT NOT NULL,
    id_patologico_personal INT NOT NULL,
    id_ginecologico INT,
    alimentacion VARCHAR(255) NOT NULL,
    vacunacion VARCHAR(255) NOT NULL,
    actividad_fisica VARCHAR(255) NOT NULL,
    aseo VARCHAR(255) NOT NULL,

    PRIMARY KEY(id_antecedente),
    FOREIGN KEY(id_enfermedad_heredofamiliar) 
        REFERENCES enfermedades_heredofamiliares(id_enfermedad_heredofamiliar),
    FOREIGN KEY(id_patologico_personal) 
        REFERENCES patologicos_personales(id_patologico_personal),
    FOREIGN KEY(id_ginecologico)
        REFERENCES ginecologicos(id_ginecologico)
);

--
-- Paciente
--

CREATE TABLE paciente(
    id_paciente INT NOT NULL AUTO_INCREMENT,
	 id_persona INT NOT NULL,
    id_antecedente INT NOT NULL,
	 ocupacion VARCHAR(40) NOT NULL,
    escolaridad VARCHAR(40) NOT NULL,
    estado_civil VARCHAR(20) NOT NULL,
	
    PRIMARY KEY(id_paciente),
    FOREIGN KEY(id_persona) REFERENCES persona(id_persona),
    FOREIGN KEY(id_antecedente) REFERENCES antecedentes(id_antecedente)
);

--
-- Especialidad
--

CREATE TABLE especialidad(
    id_especialidad INT NOT NULL AUTO_INCREMENT,
    especialidad VARCHAR(35) NOT NULL,

    PRIMARY KEY(id_especialidad)
);

--
-- Doctor
--

/* 
 * Tabla que contendrá los datos de los doctores.
 */

CREATE TABLE doctor(
    id_doctor VARCHAR(8) NOT NULL,
	id_persona INT NOT NULL,
    id_especialidad INT NOT NULL,
    cuota INT NOT NULL,
	
    PRIMARY KEY(id_doctor),
    FOREIGN KEY(id_persona) REFERENCES persona(id_persona) ON DELETE CASCADE,
    FOREIGN KEY(id_especialidad) REFERENCES especialidad(id_especialidad)
);

--
-- Cita
--

CREATE TABLE cita(
    id_cita INT NOT NULL AUTO_INCREMENT,
    id_doctor VARCHAR(8) NOT NULL,
    id_paciente INT NOT NULL,
    tipo_cita VARCHAR(11) NOT NULL,
    fecha DATE NOT NULL,
    hora TIME NOT NULL,
	
    PRIMARY KEY(id_cita),
    FOREIGN KEY(id_doctor) REFERENCES doctor(id_doctor),
    FOREIGN KEY(id_paciente) REFERENCES paciente(id_paciente)
);


--
-- Diagnostico
-- 

CREATE TABLE diagnostico(
	id_diagnostico INT NOT NULL AUTO_INCREMENT,
	diagnostico_diferencial VARCHAR(255) NOT NULL,
	diagnostico_miasmatico VARCHAR(40),
	exploracion_fisica VARCHAR(1000) NOT NULL,
	padecimiento VARCHAR(1000) NOT NULL,
	
	PRIMARY KEY(id_diagnostico)
);

--
-- Medicamento
--

CREATE TABLE medicamento(
	id_medicamento INT NOT NULL AUTO_INCREMENT,
	id_diagnostico INT NOT NULL,
	instrucciones VARCHAR(150) NOT NULL,
	nombre VARCHAR(40) NOT NULL,

	PRIMARY KEY(id_medicamento),
	FOREIGN KEY(id_diagnostico) REFERENCES diagnostico(id_diagnostico)
);

--
-- Consulta
-- 

CREATE TABLE consulta(
	id_consulta INT NOT NULL AUTO_INCREMENT,
	id_diagnostico INT NOT NULL,
	id_cita INT NOT NULL,	
	
	PRIMARY KEY(id_consulta),
	FOREIGN KEY(id_diagnostico) REFERENCES diagnostico(id_diagnostico),
	FOREIGN KEY(id_cita) REFERENCES cita(id_cita)
);

--
-- Examen
-- 

CREATE TABLE examen(
	id_examen INT NOT NULL AUTO_INCREMENT,
	id_consulta INT NOT NULL,
	examen_gabinete VARCHAR(650),
	examen_laboratorio VARCHAR(650),
	
	PRIMARY KEY(id_examen),
	FOREIGN KEY(id_consulta) REFERENCES consulta(id_consulta)
);

/*
 * VISTAS
 */
-- Doctores general <- Obtiene todos los doctores almacenados
-- Como resultado muestra: cédula, nombre completo y especialidad
CREATE VIEW doctores_general
AS
SELECT d.id_doctor, CONCAT(p.nombre, ' ', p.apellido_paterno_p, ' ', 
	p.apellido_materno_p) AS 'nombre', e.especialidad
FROM doctor d 
INNER JOIN persona p ON  d.id_persona = p.id_persona
INNER JOIN especialidad e ON d.id_especialidad  = e.id_especialidad;


-- Doctores completo <- Obtiene todos los campos del doctor
CREATE VIEW doctores_completo
AS
SELECT d.id_doctor, d.id_especialidad, p.nombre, p.apellido_paterno_p , p.apellido_materno_p, 
	p.edad, p.sexo, p.telefono, u.correo_electronico, u.contrasena, d.cuota
FROM doctor d
INNER JOIN persona p ON d.id_persona = p.id_persona
INNER JOIN usuario u ON p.correo_electronico = u.correo_electronico;

/*
 * PROCEDIMIENTOS ALMACENADOS
 */
USE sistema_medico;


--
-- Usuario, Persona, Doctor
--

/*
 * Procedimiento para registrar un nuevo Doctor
 * - Advertencia: Presupone que existe persona (tabla no vacia)
 * _ Discutir con el equipo si habrá una persona root o crear un registro nuevo
 * _ si la tabla persona esta vacia
 */

DELIMITER $$
CREATE PROCEDURE crear_doctor(
    -- Persona
	IN apellido_paterno_p VARCHAR(40),
	IN apellido_materno_p VARCHAR(40),
	IN nombre VARCHAR(40),
	IN edad INT(3),
	IN sexo VARCHAR(10),
	IN telefono VARCHAR(10),
	-- Usuarios
	IN correo_electronico VARCHAR(35),
	IN contrasena VARCHAR(20),
	IN tipo VARCHAR(15),
	-- Doctor
	IN id_doctor VARCHAR(8),
	IN id_especialidad INT,
	IN cuota INT
)
BEGIN
	INSERT INTO usuario(correo_electronico, contrasena, tipo)
		VALUES(correo_electronico, contrasena, tipo);
	INSERT INTO persona(correo_electronico, apellido_paterno_p, apellido_materno_p, nombre, edad,
		sexo, telefono)
		VALUES(correo_electronico, apellido_paterno_p, apellido_materno_p, nombre, edad, sexo,
		telefono);
	SET @id_p := (SELECT id_persona FROM persona ORDER BY id_persona DESC
		LIMIT 1);
	INSERT INTO doctor(id_doctor, id_persona, id_especialidad ,cuota)
		VALUES(id_doctor, @id_p, id_especialidad, cuota);
END $$
DELIMITER ;

--
-- Usuario, Persona, Paciente
--

/*
 * Procedimiento para registrar un nuevo Paciente
 * - Advertencia: Presupone que existe persona y que ya cuenta con antecendete
 *   (tablas no vacias)
 * _ Discutir con el equipo como gestionan antecedentes el resto de tablas
 * _ relacionadas (con tabla antecedentes)
 */

DELIMITER $$
CREATE PROCEDURE crear_paciente(
	-- Usuario
	IN correo_electronico VARCHAR(35),
	IN contrasena VARCHAR(20),
	IN tipo VARCHAR(15),
	-- Persona
	IN apellido_paterno_p VARCHAR(40),
	IN apellido_materno_p VARCHAR(40),
	IN nombre VARCHAR(40),
	IN edad INT(3),
	IN sexo VARCHAR(10),
	IN telefono VARCHAR(10),
	-- Paciente
	IN ocupacion VARCHAR(40),
	IN escolaridad VARCHAR(40),
	IN estado_civil VARCHAR(20)
)
BEGIN
	INSERT INTO usuario(correo_electronico, contrasena, tipo)
		VALUES(correo_electronico, contrasena, tipo);
	INSERT INTO persona(apellido_paterno_p, apellido_materno_p, nombre, edad,
		sexo, telefono)
		VALUES(apellido_paterno_p, apellido_materno_p, nombre, edad, sexo,
		telefono);
	SET @id_p := (SELECT id_persona FROM persona ORDER BY id_persona DESC
		LIMIT 1);
	SET @id_a := (SELECT id_antecedente FROM antecedentes ORDER BY id_antecedente
		DESC LIMIT 1);
	INSERT INTO paciente(id_persona, id_antecedente, ocupacion,
		escolaridad, estado_civil)
		VALUES(@id_p, @id_a, ocupacion, escolaridad, estado_civil);
END $$
DELIMITER ;

-- 
-- Antecedentes
--
DELIMITER $$
CREATE PROCEDURE antecedentes_proc (
    IN alimentacion VARCHAR(255),
    IN vacunacion VARCHAR(255),
    IN actividad_fisica VARCHAR(255),
    IN aseo VARCHAR(255),
    IN sexo VARCHAR(10)
)
BEGIN
    SET @id_heredo := (SELECT id_enfermedad_heredofamiliar FROM 
        enfermedades_heredofamiliares ORDER BY id_enfermedad_heredofamiliar 
        DESC LIMIT 1);
    SET @id_patologico := (SELECT id_patologico_personal FROM patologicos_personales
        ORDER BY id_patologico_personal DESC LIMIT 1);

    IF sexo = 'Masculino' THEN
        INSERT INTO antecedentes (id_enfermedad_heredofamiliar, 
            id_patologico_personal, alimentacion, vacunacion, actividad_fisica, 
            aseo) 
                VALUES (@id_heredo, @id_patologico, alimentacion, vacunacion, 
                    actividad_fisica, aseo);
    ELSE
        SET @id_gine := (SELECT id_ginecologico FROM ginecologicos 
            ORDER BY id_ginecologico DESC LIMIT 1);

        INSERT INTO antecedentes (id_enfermedad_heredofamiliar, 
            id_patologico_personal, id_ginecologico, alimentacion, vacunacion, 
            actividad_fisica, aseo) 
                VALUES (@id_heredo, @id_patologico, @id_gine, alimentacion, vacunacion, 
                    actividad_fisica, aseo);
    END IF;
END$$
DELIMITER ;

--
-- Enfermedades heredofamiliares
--
DELIMITER $$
CREATE PROCEDURE enfermedades_heredo_proc(
    IN enf_mentales VARCHAR(55),
    IN hipercolesterolemia VARCHAR(2),
    IN diabetes VARCHAR(2),
    IN hipertension VARCHAR(2),
    IN cancer VARCHAR(2),
    IN otro VARCHAR(255)
)
BEGIN
    INSERT INTO enfermedades_heredofamiliares(enfermedades_mentales, 
        hipercolesterolemia, diabetes, hipertension, cancer, otro) 
            VALUES(enf_mentales, hipercolesterolemia, diabetes,
            	hipertension, cancer, otro);
END$$
DELIMITER ;

--
-- Patológicos personales
--
DELIMITER $$
CREATE PROCEDURE patologico_person_proc(
    IN enfermedades_person VARCHAR(255),
    IN enfermedades_cronicas VARCHAR(255),
    IN tratamientos_anteriores VARCHAR(255),
    IN alergias VARCHAR(255)
)
BEGIN
    INSERT INTO patologicos_personales(enfermedades_personales, 
        enfermedades_cronicas, tratamientos_anteriores, alergias)
            VALUES(enfermedades_person, enfermedades_cronicas, 
                tratamientos_anteriores, alergias);
END$$
DELIMITER ;

--
-- Ginecológicos
--
DELIMITER $$
CREATE PROCEDURE ginecologicos_proc(
    IN menarca DATE,
    IN ritmo_menstrual VARCHAR(255),
    IN alteraciones_menstruales VARCHAR(255),
    IN gestas INT,
    IN cesareas INT,
    IN partos INT,
    IN abortos INT,
    IN anticonceptivos VARCHAR(255),
    IN inicio_menopausia DATE,
    IN trastornos_menopausicos VARCHAR(255)
)
BEGIN
    INSERT INTO ginecologicos(menarca, ritmo_menstrual, alteraciones_menstruales, 
        gestas, cesareas, partos, abortos, anticonceptivos, inicio_menopausia, 
        trastornos_menopausicos) 
            VALUES(menarca, ritmo_menstrual, alteraciones_menstruales, gestas, 
                cesareas, partos, abortos, anticonceptivos, inicio_menopausia, 
                trastornos_menopausicos);
END$$
DELIMITER ;

--
-- Diagnostico
--

/* 
 * Procedimiento que agrega valores a la tabla diagnostico y automaticamente
 * agrega su correspondiente consulta. Necesita un ID de cita.
 */
Delimiter $$
CREATE PROCEDURE nuevo_diagnostico(
	IN id_c INT, 
	IN diferencial VARCHAR(255), 
	IN miasmatico VARCHAR(40),	
	IN fisica VARCHAR(1000),
	IN pade VARCHAR(1000) 	
)
BEGIN 
	INSERT INTO diagnostico(diagnostico_diferencial, diagnostico_miasmatico,	exploracion_fisica,
	padecimiento) VALUES (diferencial,miasmatico,fisica,pade);
	SELECT DISTINCT LAST_INSERT_ID() INTO @id_d FROM diagnostico;
	INSERT INTO consulta(id_diagnostico, id_cita) 
		VALUES(@id_d,id_c);		
END $$

--
-- Examen
--

/* 
 * Procedimiento que modifica los valores de  un examen en base a su id de consulta,
 * si el examen para esa consulta no ha sido creado aun lo creara.
 * Entrada: id_consulta, examen_gabinete, examen_laboratorio
 */
Delimiter $$
CREATE PROCEDURE examen_proc(
	IN id_cons INT, 
	IN gabinete VARCHAR(650), 
	IN laboratorio VARCHAR(650)
)
BEGIN 
	SELECT id_examen INTO @id FROM examen WHERE id_consulta = 1;
	if @id IS NULL then 
		INSERT INTO examen(id_consulta, examen_gabinete, examen_laboratorio) 
			VALUES(id_cons,gabinete,laboratorio);
	ELSE 
		UPDATE examen SET 
			examen_gabinete = gabinete,
			examen_laboratorio = laboratorio
			WHERE id_examen = @id;
	END if;
END $$

--
-- Medicamento
--

/*
 * Procedimiento que agrega valores a la tabla medicamento.
 */
Delimiter $$
CREATE PROCEDURE nuevo_medicamento(
	IN id_d INT,
	IN inst VARCHAR(150),
	IN nomb VARCHAR(40)
)
BEGIN 
	INSERT INTO medicamento(id_diagnostico, instrucciones, nombre) 
		VALUES (id_d, inst, nomb);
END $$

--
-- Cita
--

/*
 * Procedimiento que agrega valores a la tabla cita,
 * verifica que el doctor seleccionado esté disponible
 */
Delimiter $$
CREATE PROCEDURE nueva_cita(
    IN id_doc VARCHAR(8),
    IN id_pac INT,
    IN tipo_c VARCHAR(11),
    IN fec DATE,
    IN hr TIME
)
BEGIN
    SELECT id_doctor INTO @id_dr FROM cita WHERE fecha = fec AND hora = hr;
	IF id_doc =!  @id_dr THEN
		INSERT INTO cita(id_doctor, id_paciente, tipo_cita, fecha, hora)
            VALUES (id_doc, id_pac, tipo_c, fec, hr);
	END if;
END $$

--
-- Consulta
--

/*
 * Procedimiento que agrega valores a la tabla consulta
 */
Delimiter $$
CREATE PROCEDURE nueva_consulta(
	IN id_di INT, 
    IN id_ci INT	
)
BEGIN 
    INSERT INTO consulta(id_diagnostico, id_cita) 
		VALUES (id_di, id_ci);
END $$

--
-- Especialidad
--

/*
 * Procedimiento que agrega valores a la tabla Especialida
 */
DELIMITER $$
CREATE PROCEDURE nueva_especialidad(
    IN esp VARCHAR(15)
)
BEGIN
    INSERT INTO especialidad(especialidad)
        VALUES(esp);
END $$

-- Obtener doctores
/*
 * Procedimiento para recuperar todos los doctores en la base de datos.
 */
DELIMITER $$
CREATE PROCEDURE obtener_doctores()
BEGIN
    SELECT * FROM doctores_general;
END $$

-- Eliminar doctor
/*
 * Procedimiento para eliminar un doctor.
 */
DELIMITER $$
CREATE PROCEDURE eliminar_doctor(
    IN id_doc VARCHAR(8)
)
BEGIN
    SET @id_persona := (SELECT id_persona FROM doctor WHERE id_doctor = id_doc);
	SET @email := (SELECT correo_electronico FROM persona WHERE id_persona = @id_persona);
	
    DELETE FROM doctor WHERE id_doctor = id_doc;
   	DELETE FROM persona WHERE id_persona = @id_persona;
   	DELETE FROM usuario WHERE correo_electronico = @email;
END $$

-- Eliminar doctor
/*
 * Procedimiento para recuperar un doctor.
 */
DELIMITER $$
CREATE PROCEDURE obtener_doctor(
    IN id_doc VARCHAR(8)
)
BEGIN
    SELECT * FROM doctores_completo WHERE id_doctor = id_doc;
END $$

-- Editar doctor
/*
 * Procedimiento para editar un doctor
 */
DELIMITER $$
CREATE PROCEDURE editar_doctor(
    -- Persona
	IN apellido_paterno VARCHAR(40),
	IN apellido_materno VARCHAR(40),
	IN nomb VARCHAR(40),
	IN anios INT(3),
	IN genero VARCHAR(10),
	IN tel VARCHAR(10),
	-- Usuarios
	IN email VARCHAR(35),
	IN contra VARCHAR(20),
	IN tip VARCHAR(15),
	-- Doctor
	IN id_doc VARCHAR(8),
	IN id_esp INT,
	IN costo INT
)
BEGIN
    SET @id_persona := (SELECT id_persona FROM doctor WHERE id_doctor = id_doc);
    SET @original_email := email;
    
    UPDATE usuario SET correo_electronico = email, contrasena = contra,
        tipo = tip WHERE correo_electronico = @original_email;

    UPDATE persona SET apellido_paterno_p = apellido_paterno, 
        apellido_materno_p = apellido_materno, nombre = nomb, edad = anios,
        sexo = genero, telefono = tel WHERE id_persona = @id_persona;
	
	UPDATE doctor SET id_doctor = id_doc, id_especialidad = id_esp, 
        cuota = costo WHERE id_doctor = id_doc;
END $$
DELIMITER ;

INSERT INTO especialidad(especialidad) VALUES("Medicina general");
INSERT INTO especialidad(especialidad) VALUES("Medicina interna");
INSERT INTO especialidad(especialidad) VALUES("Neurología");
INSERT INTO especialidad(especialidad) VALUES("Otorrinolaringología");


