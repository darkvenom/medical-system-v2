// Imports
const { ipcRenderer, ipcMain } = require('electron');

// General information
let name = document.getElementById('nameInput');
let paternLastName = document.getElementById('paternLastNameInput');
let motherLastname = document.getElementById('motherLastNameInput');
let age = document.getElementById('ageInput');
let gender = document.getElementById('genderSelect');
let phone = document.getElementById('phoneInput');
let email = document.getElementById('emailInput');
let password = document.getElementById('passwordInput');

// Doctor information
let doctorId = document.getElementById('doctorIdInput');
let specialtyId = document.getElementById('specialtySelect');
let price = document.getElementById('priceInput');

// Table
let doctorsTable = document.getElementById('doctorsTable');

// Forms
let doctorForm = document.getElementById('doctorInfoForm');
let generalForm = document.getElementById('generalDataForm');

// Buttons
const saveBtn = document.getElementById('saveBtn');
const cancelBtn = document.getElementById('cancelBtn');

// Edit
let isEditing = false;

class Person {
    constructor(fLastName, mLastName, name, age, gender, phone, email, password, type) {
        this.fLastName = fLastName;
        this.mLastName = mLastName;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.type = type;
    }
}

class Doctor extends Person {
    constructor(doctorId, specialtyId, price, fLastName, mLastName, name, age,
        gender, phone, email, password, type) {
        super(fLastName, mLastName, name, age, gender, phone, email, password, type);
        this.doctorId = doctorId;
        this.specialtyId = specialtyId;
        this.price = price;
    }
}


// Listeners
saveBtn.addEventListener('click', (e) => {
    e.preventDefault();

    let doctor = new Doctor(doctorId.value, specialtyId.value, price.value, 
        paternLastName.value, motherLastname.value, name.value, age.value, 
        gender.value, phone.value, email.value, password.value, 0);
    
    if(!isEditing) {
        if(!existsId(doctor.doctorId)) {
            let msgStatus = ipcRenderer.send('add-doctor', doctor);
            alert('¡El doctor ha sido registrado exitosamente!');
        } else {
            alert('La cédula profesional ' + doctor.doctorId + ' ya se encuentra registrada.');
            doctorId.focus();
        }
    } else {
        ipcRenderer.send('update-doctor', doctor);
        alert('¡El doctor se ha actualizado exitosamente!');

        doctorId.disabled = false;
        isEditing = false;
    }

    
    render();
    doctorId.focus();
    generalForm.reset();
    doctorForm.reset();
});

cancelBtn.addEventListener('click', (e) => {
    e.preventDefault();
    
    doctorId.disabled = false;
    isEditing = false;

    render();
    generalForm.reset();
    doctorForm.reset();
})


function existsId(doctorId) {
    let doctors = doctorsTable.children[1].children;
    let doctorIdExists = false;

    Array.from(doctors).forEach(doctor => {
        const id = doctor.children[0].innerText;
        console.log(doctorId === id);

        if(id === doctorId) doctorIdExists = true;
    });

    return doctorIdExists;
}

function deleteDoctor(doctorId) {
    const deleteMsg = confirm('¿Deseas eliminar el doctor con la cédula: ' + doctorId + '?');

    if(deleteMsg) {
        ipcRenderer.send('delete-doctor', doctorId);
        render();
        alert('Eliminación exitosa');
    } 
}

function editDoctor(doctorIdArg) {
    let doctor = ipcRenderer.sendSync('get-doctor', doctorIdArg);
    
    doctorId.value = doctor.id_doctor;  
    specialtyId.value = doctor.id_especialidad;
    price.value = doctor.cuota;
    paternLastName.value = doctor.apellido_paterno_p;
    motherLastname.value = doctor.apellido_materno_p; 
    name.value = doctor.nombre;
    age.value = doctor.edad; 
    gender.value = doctor.sexo;
    phone.value = doctor.telefono;
    email.value = doctor.correo_electronico; 
    password.value = doctor.contrasena;

    doctorId.disabled = true;
    isEditing = true;
}

function render() {
    let doctors = ipcRenderer.sendSync('get-doctors');
    let tBody = doctorsTable.children[1];

    // Init table
    tBody.innerHTML = '';

    // Fill table
    doctors.forEach(doctor => {
        tBody.innerHTML += `<tr>
                <td>${doctor.id_doctor}</td>
                <td>${doctor.nombre}</td>
                <td>${doctor.especialidad}</td>
                <td>
                    <button class="btn btn-sm btn-secondary"
                        onclick="editDoctor('${doctor.id_doctor}')">
                        Modificar
                    </button>

                    <button class="btn btn-sm btn-danger" 
                        onclick="deleteDoctor('${doctor.id_doctor}')">
                        Eliminar
                    </button>
                </td>
            </tr>`;
    });
}

render();