class Person {
    constructor(fLastName, mLastName, name, age, gender, phone, email, password, type) {
        this.fLastName = fLastName;
        this.mLastName = mLastName;
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.phone = phone;
        this.email = email;
        this.password = password;
        this.type = type;
    }
}

class Doctor extends Person {
    constructor(doctorId, specialtyId, price, fLastName, mLastName, name, age,
        gender, phone, email, password, type) {
        super(fLastName, mLastName, name, age, gender, phone, email, password, type);
        this.doctorId = doctorId;
        this.specialtyId = specialtyId;
        this.price = price;
    }
}

export { Doctor };