const mysql = require('mysql');

let connection = mysql.createConnection({
    host: 'localhost',
    user: 'personal_medico',
    password: '123qwerty',
    database: 'sistema_medico',
    port: 3306
});

function getConnection() {
    return connection;
}

module.exports = {
    getConnection
};