const { app, BrowserWindow, Notification } = require('electron');
const { ipcMain } = require('electron');
const {getConnection} = require('./database.js');

// Intern process controller
// Add doctor process
ipcMain.on('add-doctor', (event, doctor) => {
    let msg = '';
    const conn = getConnection();
    
    const query = 'call crear_doctor(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
    conn.query(query, Object.values(doctor), function(err, result, field){
        if(err) {
            console.error('error: ', err);
            return;
        }
    });

    showNotification('¡Doctor registrado!', 'El doctor se ha almacenado exitosamente.');
});

// Get all doctors
ipcMain.on('get-doctors', (event) => {
    const conn = getConnection();
    const query = 'CALL obtener_doctores()';

    conn.query(query, (err, doctors) => {
        if(err) {
            console.error(err);
            return;
        }

        event.returnValue = doctors[0];
    });
});

ipcMain.on('delete-doctor', (event, doctorId) => {
    const conn = getConnection();
    const query = 'CALL eliminar_doctor(?)';
    
    conn.query(query, doctorId, (err) => {
        if(err) {
            console.error(err);
            return;
        }
    });

    showNotification('¡Doctor eliminado!', 'El doctor se ha eliminado exitosamente.');
});

ipcMain.on('get-doctor', (event, doctorId) => {
    const conn = getConnection();
    const query = 'CALL obtener_doctor(?)';

    conn.query(query, doctorId, (err, doctor) => {
        if(err) {
            console.error(err);
            return;
        }

        event.returnValue = doctor[0][0];
    });
});

ipcMain.on('update-doctor', (event, doctor) => {
    const conn = getConnection();
    const query = "CALL editar_doctor(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    conn.query(query, Object.values(doctor), (err) => {
        if(err) {
            console.error(err);
            return;
        }
    });

    showNotification('¡Información actualizada!', 'La información del doctor ' + 
        'ha sido actualizada exitosamente');
});

function showNotification(notificationTitle, notificationBody) {
    new Notification({
        title: notificationTitle,
        body: notificationBody
    }).show();
}

// Function to create the main window
function createWindow() {
    let window = new BrowserWindow({
        width: 1280,
        height: 600,
        icon: __dirname + '/icons/icon.ico',
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false
        }
    });

    window.loadFile('doctor-ui/index.html');
}

app.whenReady().then(() => {
    createWindow();
    
    app.on('window-all-closed', () => {
        if(process.platform !== 'darwin') app.quit();
    })
});